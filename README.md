# Clareply Convenience API Documentation

*version 1.0.1*  
*2019-06-10*

Use the Clareply Convenience API to create document requests – then grab the documents once they have arrived. 

This API is designed as a programmable alternative to using the [Clareply app](https://beta.app.clareply.com). Some features of the app are not available through this API. 



#### HTTP requests
All endpoints are called as HTTP POST commands. 

Parameters are sent in the request body as keys of a JSON object. The request body `Content-Type` header should be set to `application/json`.

Unless otherwise noted, all response bodies are JSON-encoded objects containing the keys mentioned in the documentation for the particular endpoint. 

All JSON response bodies contain the following keys:

- **success** (boolean), which denotes whether or not the action was completed.
- **error** (string), contains a recognizable error key if an error occurred while performing the action
- **error_data** (object), which in some cases contains data about the error that might be useful for debugging. 

No session state is stored on the server between each request.



#### Authentication
All endpoints require you to send the following parameters for the user you want to act on behalf of:

- **orgname** (e.g. "clareply")
- **username** (e.g. "shum")
- **passphrase** (e.g. "holypenguin")

Make sure the particular user account has the ***API access*** permission enabled. 

You can set user permissions through the [Clareply app](https://beta.app.clareply.com). Once you're logged in as a user with *user administration* permissions, click **Settings** (in the lower-left corner of the screen), then under the Organization header click **Users**, then click on a user in the list of users, then scroll to the **Permissions** part and enable or disable the ***API access*** permission. 

*Note: There is not yet an OpenAPI definition file for this API. If one would be of use to you, feel free to create one and submit a pull request to this repository.*


#### Security level
Using this API involves submitting a user password to the server, which enables the server to decrypt documents right before transmitting them back to the user. Though the Convenience API does its best to forget passwords and decrypted data between calls, this means that documents are strictly speaking not end-to-end-encrypted when accessed through this API, whereas they are when using the Clareply app.



## Endpoints: 

All endpoints are located at `https://api.clareply.com/conv-api/`. Use of HTTPS is required.

### /v1/create-case-and-request
In one swing, create a case, add people to the case, and create requests for documentation for all of them.
	
#### parameters:
	client: (Entity) {
		name: (string) (name of person or company)
		type: (string) ("Person" or "Company")
		contactInfo: (array of objects) (only used if type == "Person") [
			{ 
				type: (string) ("email" or "phone"), 
				email: (string) (an email address), 
				phone: (string) (a phone number, always including country code. Spaces and "+" sign are allowed) 
			},
			…
		],
		request: (true || RequestDefinition)
		
		owners: (array<Entity>) (only applicable if type == "Company"),
		contacts: (array<Entity>) (only applicable if type == "Company"),
	}

The `owners` and `contacts` arrays are lists of objects following the same *Entity* structure as the top-level `client` object. Note that an owner can itself be a company with owners, allowing you to store ownership resembling a tree structure.

The `contacts` *array* should only include entities with `type == "Person"`. 

Set `request` to true on an *Entity* with `type == "Person"`, if you want to request information from them. 

Set `request` to true on an *Entity* with `type == "Company"`, if you want to request identity documents and missing contact details from the Company's owners (including owners' owners, if any) and missing contact details from the Company's contacts (if any).

Set `request` to true for the top-level `client` if you want to request identity documents and/or contact details from all owners and contacts.

In a future version of this API, it will be possible to define which documents you wish to obtain by setting the value of `request` to an object following the yet-to-be-finalized *RequestDefinition* specification. 


#### result:
- **entityId**: (string) id of the created Case entity, for later reference.




***
### /v1/get-cases-and-documents
Used to fetch a list of cases, their involved people and information about their documents and ongoing requests.

#### parameters:
	filters: (object, optional) {
		documentationStatus: (optional) (string) (),
		reachRequestStatus: (optional) (string),
	}
	entityId (string, optional) (use this to fetch a specific Case, Company or Person)
	
	
#### result:
	results: (array<Entity>) [
		{
			entityId: (string),
			type: (string) ("Case", "Company" or "Person"),
			
			documentationStatus: (string) ("waiting", "pending", "approved"),
			reachRequestStatus: (string) ("pending", "fulfilled"),
			
			client: (array<Entity>) (if the Entity has type == "Case"),
			owners: (array<Entity>) (if the Entity has type == "Company"),
			contacts: (array<Entity>) (if the Entity has type == "Case" or "Company"),
			
			documents: (array<Document>) (if the Entity has type == "Person") [
				{
					documentId: (string),
					type: (string) (examples: "passport", "driversLicense", "dk_sundhedskort". Note: Type names may change in a future version of the API),
					mimeType: (string, mime-type),
					payloadSize: (int, payload size in bytes)
					
					approval: (string) ("pending", "approved" or "rejected"),
					metadata: (object),
					timeCreated: (microtime),
					timeLastEdited: (microtime),
				},
				...
			],
			requests: (array<Request>) (if the Entity has type == "Person") [
				{
					requestId: (string),
					requestDefinition: (RequestDefinition – not implemented yet)
					contactInfoRequested: (boolean) (derived from internal request definition),
					documentsRequested: (boolean) (derived from internal request definition),
					timeCreated: (microtime),
				},
				...
			],
			contactPoints: (array<ContactPoint>) (if the Entity has type == "Person") [
				{
					url: (string) (the URL that has been sent to the user),
					type: (string) ("email" or "phone"),
					email: (string, optional),
					phone: (string, optional),
					messageType: (string) ("initialMessage", "userActivatedReminder", "startReminder", "resumeReminder"),
					timeCreated: (microtime),
					timeToSend: (microtime),
					timeSent: (microtime),
					timeCancelled: (microtime),
					timeFailed: (microtime) (only applies when type == 'phone'),
					timeDelivered: (microtime) (only applies when type == 'phone'),
					_decryption_error: (boolean) (true if the ConvenienceAPI was not able to decrypt the contactInfo related to this contactPoint)
				},
				...
			],

			
		},
		...
	]

**`client`**, **`owners`** and **`contacts`** are arrays of objects that follow the same Entity format as is outlined above.

**`documentationStatus`** can have one of the following values:

- `waiting` when no documents are stored.
- `pending` when documents have been received and are awaiting approval.
- `approved` when documents necessary to fulfill AML requirements have been approved.

**`reachRequestStatus`** can have one of the following values:

- `pending` when a request has been created, but not necessarily sent.
- `fulfilled` when requested documents have been received.

**`documents`** are a list of records reflecting documents that have been entered into the system, either by a client/customer through the Reach web app, or by an in-house user of the Clareply app. *(Note: to retrieve the actual contents of each document, you need to use the /v1/get-document-payload endpoint described below.)*

**`requests`** are a list of "wishes" for information.

**`contactPoints`** are a list of instances where we have contacted a person.




***
### /v1/get-document
Used to fetch information about a document with a known documentId.

Note: The payload of the document (i.e., its contents) is not fetched with this endpoint.

#### parameters:
- **documentId**: (string)
	
#### result:
	document: (Document) {
		documentId: (string),
		entityId: (string),
		
		type: (string) (examples: "passport", "driversLicense", "dk_sundhedskort". Note: Type names may change in a future version of the API),
		mimeType: (string, mime-type),
		payloadSize: (int, payload size in bytes)
					
		approval: (string) ("pending", "approved" or "rejected"),
		metadata: (object),
		timeCreated: (microtime),
		timeLastEdited: (microtime),
	}	




***
### /v1/get-document-payload
Used for fetching the contents of a document.

#### parameters:
- **documentId**: (string)
	
#### result:
This endpoint returns raw binary data instead of a JSON object. The `Content-Type` header of the response will be configured with a content-appropriate MIME type. 




***
### /v1/update-document
Used to change the approval status of a document. 
#### parameters:
- **documentId**: (string)
- **approval**: (string) ("pending", "approved" or "rejected")

#### result:
Nothing apart from the standard `success` and `error` keys outlined in the introduction of this document.


