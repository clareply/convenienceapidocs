# Examples

## /v1/create-case-and-request
### URL:
https://api.clareply.com/conv-api/v1/create-case-and-request

### Request body, simple client:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "client": {
	    "name": "PersonName",
	    "type": "Person",
	    "contactInfo": [
	      {
	        "type": "email",
	        "email": "info@clareply.com"
	      },
	      {
	        "type": "phone",
	        "phone": "12345678"
	      }
	    ],
	    "request": true
	  }
	}

### Request body, complex client with known contact info:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "client": {
	    "name": "CompanyName",
	    "type": "Company",
	    "owners": [
	      {
	        "name": "PersonName",
	        "type": "Person",
	        "contactInfo": [
	          {
	            "type": "email",
	            "email": "info@clareply.com"
	          },
	          {
	            "type": "phone",
	            "phone": "12345678"
	          }
	        ]
	      },
	      {
	        "name": "AnotherCompanyName",
	        "type": "Company",
	        "owners": [
	          {
	            "name": "AnotherPersonName",
	            "type": "Person"
	          },
	          {
	            "name": "YetAnotherPersonName",
	            "type": "Person"
	          }
	        ]
	      }
	    ],
	    "request": true
	  }
	}

### Request body, complex client with separate contact person:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "client": {
	    "name": "CompanyName",
	    "type": "Company",
	    "owners": [
	      {
	        "name": "PersonName",
	        "type": "Person",
	      },
	      {
	        "name": "AnotherCompanyName",
	        "type": "Company",
	        "owners": [
	          {
	            "name": "AnotherPersonName",
	            "type": "Person"
	          },
	          {
	            "name": "YetAnotherPersonName",
	            "type": "Person"
	          }
	        ]
	      }
	    ],
	    "contacts": [
	      {
	        "name": "ContactPersonName",
	        "type": "Person",
	        "contactInfo": [
	          {
	            "type": "email",
	            "email": "info@clareply.com"
	          }
	        ]
	      }
	    ],
	    "request": true
	  }
	}
	

	
***
## /v1/get-cases-and-documents
### URL:	
https://api.clareply.com/conv-api/v1/get-cases-and-documents

### Request body, specific case:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "entityId": 456
	}

### Request body, list of cases with filter:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "filters": {
	    "documentationStatus": "pending"
	  }
	}

***
## /get-document
### URL:	
https://api.clareply.com/conv-api/v1/get-document
### Request body:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "documentId": 345
	}
	
***
## /update-document
### URL:	
https://api.clareply.com/conv-api/v1/update-document
### Request body:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "documentId": 345,
	  "approval": "approved"
	}

***
## /get-document-payload
### URL:	
https://api.clareply.com/conv-api/v1/get-document-payload
### Request body:
	{
	  "orgname": "api-demo", 
	  "username": "demo", 
	  "passphrase": "xxx",
	  "documentId": 345
	}
	